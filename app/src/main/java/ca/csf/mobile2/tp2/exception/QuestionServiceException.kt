package ca.csf.mobile2.tp2.exception

class QuestionServiceException(message : String) : Exception(message) {
}