package ca.csf.mobile2.tp2.question

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Question(
    @JsonProperty("choice1") val choice1: String = "",
    @JsonProperty("choice2") val choice2: String = "",
    @JsonProperty("id") val id: String = "",
    @JsonProperty("nbChoice1") val nbChoice1: Int = 0,
    @JsonProperty("nbChoice2") val nbChoice2: Int = 0,
    @JsonProperty("text") val text: String = ""
) : Parcelable
