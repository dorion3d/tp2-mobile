package ca.csf.mobile2.tp2.question

enum class AskQuestionActivityState {
    ASKQUESTION,
    DISPLAYRESULTS,
    LOADING,
    ERROR_RETRY
}
