package ca.csf.mobile2.tp2.question

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ViewModel(var question: Question) : BaseObservable(), Parcelable {

    private val percentageFormat: String = "%.0f%%"

    var appState: AskQuestionActivityState = AskQuestionActivityState.ASKQUESTION
        @Bindable get() {
            return field
        }
        set(value) {
            field = value
            notifyChange()
        }

    private val totalNbOfAnswers: Int
        get() {
            return question.nbChoice1 + question.nbChoice2
        }

    val questionTxt: String
        @Bindable get() {
            return question.text
        }

    val choice1Txt: String
        @Bindable get() {
            return question.choice1
        }

    val choice2Txt: String
        @Bindable get() {
            return question.choice2
        }

    val nbChoice1Txt: String
        @Bindable get() {
            return toAnswerPercentageString(question.nbChoice1)
        }

    val nbChoice2Txt: String
        @Bindable get() {
            return toAnswerPercentageString(question.nbChoice2)
        }

    fun setNewQuestion(newQuestion: Question) {
        question = newQuestion
        notifyChange()
    }

    private fun toAnswerPercentageString(i: Int): String {
        var percent = 0f
        if (totalNbOfAnswers != 0)
            percent = (i * 100 / totalNbOfAnswers).toFloat() // Aprouvé par Dan pour le 100

        return percentageFormat.format(percent)
    }
}