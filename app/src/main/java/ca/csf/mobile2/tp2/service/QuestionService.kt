package ca.csf.mobile2.tp2.service

import ca.csf.mobile2.tp2.R
import ca.csf.mobile2.tp2.exception.QuestionServiceException
import ca.csf.mobile2.tp2.question.Question
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.androidannotations.annotations.EBean
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import java.lang.Exception

private val BASE_URL = "http://10.0.2.2:8080/"

@EBean(scope = EBean.Scope.Singleton)
class QuestionService {

    interface Service {
        @GET("api/v1/question/random")
        fun getRandomQuestion(): Call<Question>

        @POST("api/v1/question/{id}/choose1")
        fun chooseFirstChoiceOfQuestion(@Path("id") id: String): Call<Question>

        @POST("api/v1/question/{id}/choose2")
        fun chooseSecondChoiceOfQuestion(@Path("id") id: String): Call<Question>
    }

    private val service: Service

    init {
        val jackson = ObjectMapper().registerKotlinModule()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(jackson))
            .build()

        service = retrofit.create(Service::class.java)
    }

    fun getRandomQuestion(): Question {
        try {
            val call: Call<Question> = service.getRandomQuestion()

            val response: Response<Question> = call.execute()

            if (response.isSuccessful) {
                return response.body()!!
            } else {
                throw QuestionServiceException(R.string.text_error_server.toString())
            }
        } catch (e: Exception) {
            throw QuestionServiceException(R.string.text_error_internet.toString())
        }
    }

    fun chooseFirstChoiceOfQuestion(id: String): Question {
        try {
            val call: Call<Question> = service.chooseFirstChoiceOfQuestion(id)

            val response: Response<Question> = call.execute()

            if (response.isSuccessful) {
                return response.body()!!
            } else {
                throw QuestionServiceException(R.string.text_error_server.toString())
            }
        } catch (e: Exception) {
            throw QuestionServiceException(R.string.text_error_internet.toString())
        }
    }

    fun chooseSecondChoiceOfQuestion(id: String): Question {
        try {
            val call: Call<Question> = service.chooseSecondChoiceOfQuestion(id)

            val response: Response<Question> = call.execute()

            if (response.isSuccessful) {
                return response.body()!!
            } else {
                throw QuestionServiceException(R.string.text_error_server.toString())
            }
        } catch (e: Exception) {
            throw QuestionServiceException(R.string.text_error_internet.toString())
        }
    }
}