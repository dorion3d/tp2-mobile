package ca.csf.mobile2.tp2.question

import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import ca.csf.mobile2.tp2.R
import ca.csf.mobile2.tp2.databinding.ActivityAskQuestionBinding
import ca.csf.mobile2.tp2.service.QuestionService
import org.androidannotations.annotations.*

@DataBound
@EActivity(R.layout.activity_ask_question)
class AskQuestionActivity : AppCompatActivity() {

    @BindingObject
    protected lateinit var binding: ActivityAskQuestionBinding

    @ViewById(R.id.question)
    protected lateinit var questionTxt: TextView

    @ViewById(R.id.errorTextView)
    protected lateinit var errorTextView: TextView

    @InstanceState
    protected lateinit var viewModel: ViewModel

    @Bean
    protected lateinit var questionService: QuestionService

    private final var lastServiceCalled: (() -> Unit)? = null

    @JvmField
    @InstanceState
    protected final var appState: AskQuestionActivityState = AskQuestionActivityState.ASKQUESTION

    @AfterViews
    protected fun onViewsInjected() {
        if (!this::viewModel.isInitialized) {
            viewModel = ViewModel(Question())
            getRandomQuestion()
        }

        binding.data = viewModel
    }

    @Background
    protected fun getRandomQuestion() {
        viewModel.appState = AskQuestionActivityState.LOADING
        lastServiceCalled = { getRandomQuestion() }
        try {
            viewModel.setNewQuestion(questionService.getRandomQuestion())
            viewModel.appState = AskQuestionActivityState.ASKQUESTION
        } catch (e: Exception) {
            changeErrorShown(e.message!!.toInt())
            viewModel.appState =AskQuestionActivityState.ERROR_RETRY
        }
    }

    @Background
    protected fun chooseOption(choice: Int) {
        viewModel.appState = AskQuestionActivityState.LOADING

        try {
            when (choice) {
                1 -> {
                    lastServiceCalled = { chooseOption(1) }
                    viewModel.setNewQuestion(questionService.chooseFirstChoiceOfQuestion(viewModel.question.id))
                }
                2 -> {
                    lastServiceCalled = { chooseOption(2) }
                    viewModel.setNewQuestion(questionService.chooseSecondChoiceOfQuestion(viewModel.question.id))
                }
            }
            viewModel.appState = AskQuestionActivityState.DISPLAYRESULTS
        } catch (e: Exception) {
            changeErrorShown(e.message!!.toInt())
            viewModel.appState = AskQuestionActivityState.ERROR_RETRY
        }
    }

    @Click(R.id.choice1Button)
    protected fun onChoice1BtnClick() {
        chooseOption(1)
    }

    @Click(R.id.choice2Button)
    protected fun onChoice2BtnClick() {
        chooseOption(2)
    }

    @Click(R.id.choice1ResultBackground)
    protected fun onChoice1ResultBtnClick() {
        getRandomQuestion()
    }

    @Click(R.id.choice2ResultBackground)
    protected fun onChoice2ResultBtnClick() {
        getRandomQuestion()
    }

    @Click(R.id.retryButton)
    protected fun onRetryBtnClick() {
        lastServiceCalled?.invoke()
    }

    @UiThread
    protected fun changeErrorShown(stringId: Int) {
        errorTextView.text = getString(stringId)
    }
}


